var car_year= require('/home/deepanshu/Documents/Mountblue/JavaScript/JSDrill-Car/problem4.js');
module.exports = function oldCars( inventory )
{
    if( inventory==null || inventory===undefined || inventory.length === 0 || Object.keys(inventory).length===0)
    {
        return [];
    }
    var old_car=[];
    var years=car_year(inventory);
    for(let i=0;i<years.length;i++)
    {
        if(years[i]<2000)
        old_car.push(years[i]);
    }
    return old_car;
}
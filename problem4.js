module.exports = function carYear( inventory )
{
    if( inventory==null || inventory===undefined || inventory.length === 0 || Object.keys(inventory).length===0)
    {
        return [];
    }
    var car_year=[];
    for(let i=0;i<inventory.length;i++)
    {
        car_year.push(inventory[i].car_year);
    }
    return car_year;
}

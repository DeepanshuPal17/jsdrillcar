module.exports = function lastCar(inventory)
{
    if( inventory==null || inventory===undefined || inventory.length === 0 || Object.keys(inventory).length===0)
    {
        return [];
    }
    return inventory[inventory.length-1];
}
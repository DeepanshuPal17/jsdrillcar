
module.exports = function getCarInfoById( inventory ,id)
{
    if( inventory==null || inventory==undefined || inventory.length == 0 || Object.keys(inventory).length===0)
    {
        return [];
    }

    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].id===id)
        {
            return inventory[i];
        }
    }
    return [];
}

module.exports = function getAudiandBmw( inventory)
{
    if( inventory==null || inventory===undefined || inventory.length === 0 || Object.keys(inventory).length===0)
    {
        return [];
    }
    var AudiandBmw=[];
    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_make==="Audi"|| inventory[i].car_make==="BMW")
        AudiandBmw.push(inventory[i]);
    }
    return AudiandBmw;
}
module.exports = function sortedCarModels( inventory)
{
    if( inventory==null || inventory===undefined || inventory.length === 0 || Object.keys(inventory).length===0)
    {
        return [];
    }
    function compare(value1,value2)
    {
        return (value1.car_model).localeCompare(value2.car_model); 
    }
    inventory.sort(compare);

    return inventory;
}